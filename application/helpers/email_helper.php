<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * getNotificationTheme()
 * 
 * @param mixed $message
 * @param mixed $subject
 * @param string $filePath
 * @return
 */
$CI = & get_instance();

//get the email theme from data base 
function getEmailTheme($emailTitle, $subject, $message, $fileContents, $name) {
 
    // Call Codeigniter Instance
    $baseURL = HTTP_PATH;
    $siteName = SITE_NAME;
    
    $searchPatterns[0] = "@XcurrentDateX@";
    $searchPatterns[1] = '@XsubjectX@';
    $searchPatterns[2] = '@XcontentsX@';
    $searchPatterns[3] = '@XhttpPathX@';
    $searchPatterns[4] = '@XsiteNameX@';
    $searchPatterns[5] = '@XmailTitleX@';
    $searchPatterns[6] = '@XuserNameX@';

    //Replace with this values
    //$replaceBy[0] = date(FRONTEND_DATE_VIEW_FORMAT);
    $replaceBy[0] = date("Y/m/d");
    $replaceBy[1] = $subject;
    $replaceBy[2] = $message;
    $replaceBy[3] = $baseURL;
    $replaceBy[4] = $siteName;
    $replaceBy[5] = $emailTitle;
    $replaceBy[6] = $name;

    //Return the theme processed contents.
    return preg_replace($searchPatterns, $replaceBy, $fileContents);
}

//get email template from notification file
function getNotificationTheme($emailTitle, $subject, $message) {
    // Call Codeigniter Instance
    $baseURL = HTTP_PATH;
    $siteName = SITE_NAME;

    //Notification themem file path.
    $filePath = str_replace("\\", "/", dirname(dirname(dirname(__FILE__)))) . "/application/views/emails/notification.html";

    //Get HTML contents of theme file.
    $fileContents = file_get_contents($filePath);

    //Search with this patterns
    $searchPatterns[0] = '<<!--currentdate-->>';
    $searchPatterns[1] = '<<!--subject-->>';
    $searchPatterns[2] = '<<!--contents-->>';
    $searchPatterns[3] = '<<!--baseURL-->>';
    $searchPatterns[4] = '<<!--siteName-->>';
    $searchPatterns[5] = '<<!--emailTitle-->>';

    //Replace with this values
    //$replaceBy[0] = date(FRONTEND_DATE_VIEW_FORMAT);
    $replaceBy[0] = date("Y/m/d");
    $replaceBy[1] = $subject;
    $replaceBy[2] = $message;
    $replaceBy[3] = $baseURL;
    $replaceBy[4] = $siteName;
    $replaceBy[5] = $emailTitle;

    //Return the theme processed contents.
    return preg_replace($searchPatterns, $replaceBy, $fileContents);
}

function sendMail($to, $msg, $subject, $cc=null) {
  
   $url = 'https://api.sendgrid.com/';
        $user = 'biznet';
        $pass = 'uniway23';
        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $to,
            'subject'   => $subject,
            'html'      => $msg,
            'text'      => $msg,
            'from'      => 'noreply@biznetafrica.com',
            'fromname'  => 'Biznet Africa',
          );
         // if($cc!=null){
           // $params['cc']="$cc";
          //}
          
        $request =  $url.'api/mail.send.json';
        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        // print everything out
        //print_r($response);
        $res= json_decode($response);
        //pr($res);
        //echo $res->message;
        if('success'===$res->message)
        {
            return true;
        }
        else
        {
            return false;
        }
}

function SignupSendMail($to, $msg, $subject, $cc=null) {
	/*$sender = "Biznet Africa";
	$sender_mail ="info@biznetafrica.com";
	//$header  = "From: info@biznetafrica.com";
        $header = "From: <info@biznetafrica.com> Biznet Africa";
        if($cc!=null){
          $header = "Cc:$cc \r\n";     
        }   
        //$header = "Cc:bssoniya@gmail.com \r\n";   
        $header .= "charset:utf-8";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-type:text/html\r\n";
        $retval = mail($to,$subject,$msg,$header);
        if( $retval == true )
        {
            return true;
        }
        else
        {
            return false;
        }*/
        
        $url = 'https://api.sendgrid.com/';
        $user = 'biznet';
        $pass = 'uniway23';
        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $to,
            'subject'   => $subject,
            'html'      => $msg,
            'text'      => $msg,
            'from'      => 'noreply@biznetafrica.com',
            'fromname'  => 'Biznet Africa',
          );
        $request =  $url.'api/mail.send.json';
        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        // print everything out
        //print_r($response);
        $res= json_decode($response);
        //pr($res);
        //echo $res->message;
        if('success'===$res->message)
        {
            return true;
        }
        else
        {
            return false;
        }
}

function sendForgotPassword($data) {
    $title = SITE_NAME;
    $subject = 'Your login detail at ' . SITE_NAME;
    $pass_text = 'Your account Password is: ' . base64_decode($data['user_password']);

    $to = $data['user_primary_email'];
    $name = $data['user_name'];
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="font-family:segoe UI, Arial, sans-serif; font-size:13px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="26" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;">
                        <strong>Hi ' . ucfirst($data['user_name']) . ',</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Tahoma, Arial, sans-serif; font-size:11px; color:#575757; line-height:15px; padding-bottom:10px;font-size:12px;">' . $pass_text . '</td>
                    </tr>';
    $message .='<tr>
                    <td height="5">
                    
                    </td>
                    </tr>';
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td>Thanks and regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);

    $check = SignupSendMail($to, $body, $subject);

    return $check;
}

function sendRegistrationMail($user_password, $email, $user_email_token, $name) {
    $title = SITE_NAME;
    $subject = 'Verify your Account';
    $pass_text = 'Your account Password is: ' . $user_password;
    $active_link_text = 'To verify your account please click here: ';
    $active_link = HTTP_PATH . 'auth/email_verification/' . $email . '/' . md5($user_email_token);
    $to = $email;
    $name = $name;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="font-family:segoe UI, Arial, sans-serif; font-size:13px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="26" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;">
                        <strong>Hi ' . ucfirst(@$name) . ',</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Tahoma, Arial, sans-serif; font-size:11px; color:#575757; line-height:15px; padding-bottom:10px;font-size:12px;">' . $pass_text . '</td>
                    </tr>
                    <tr>
                        <td style="font-family:Tahoma, Arial, sans-serif; font-size:11px; color:#575757; line-height:15px; padding-bottom:10px;font-size:12px;">' . $active_link_text . '<a href="' . $active_link . '">Verify Account.</a></td>
                    </tr>';
    $message .='<tr>
                    <td height="5">
                    
                    </td>
                    </tr>';
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td>Thanks and regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);
    $check = SignupSendMail($to, $body, $subject);
    return $check;
}

function sendCustomEmail($to, $msg, $sub, $name) {
    $title = SITE_NAME;
    $subject = $sub;
    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="font-family:segoe UI, Arial, sans-serif; font-size:13px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="26" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;">
                        <strong>Hi ' . ucfirst(@$name) . ',</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Tahoma, Arial, sans-serif; font-size:11px; color:#575757; line-height:15px; padding-bottom:10px;font-size:12px;">' . $msg . '</td>
                    </tr>';
    $message .='<tr>
                    <td height="5">
                    
                    </td>
                    </tr>';
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">
                ';
    $message .= '<td>Thanks and regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';
    $body = getNotificationTheme($title, $subject, $message);
    $check = sendMail($to, $body, $subject);
    return $check;
}

/*
 *   @Change Password Notification
 */

function _changePasswordEmail($dataArray) {

    $to = $dataArray['ToEmail'];
    $name = $dataArray['UserfullName'];

    $emailId = $dataArray['ToEmail'];
    $password = $dataArray['userpassword'];
    $siteURL = HTTP_PATH;
    $subject = "Reset Password Notification";
    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="font-family:segoe UI, Arial, sans-serif; font-size:13px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="26" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;">
                        <strong> Hello, ' . ucwords(@$name) . '</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Tahoma, Arial, sans-serif; font-size:11px; color:#575757; line-height:15px; padding-bottom:10px;font-size:12px;">Please go through your updated Password details: </td>
                    </tr>';
    $message .='<tr>
                    <td height="5">
                    </td>
                    </tr>
                    <tr>
                    <td align="left">
                        <table width="287" border="0" bgcolor="#D23D3D" cellspacing="1" cellpadding="6" style="border:solid 3px #D23D3D;">
                        <tr>
                            <td colspan="2">
                            <strong style="color:#FFF;">Login Information</strong>
                            </td>
                        </tr>
                        <tr>';
    $message .= '<td bgcolor="#ffffff" width="100" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;"><strong>Email ID:</strong></td>';
    $message .= '<td width="270" bgcolor="#ffffff" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;">' . @$emailId . '</td>';
    $message .= '</tr>';
    $message .= '<tr>';
    $message .= '<td  bgcolor="#ffffff" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;"><strong>Password:</strong></td>';
    $message .= '<td  bgcolor="#ffffff" style="font-family:Tahoma, Arial, sans-serif; font-size:12px;color:#575757;">' . @$password . '</td>';
    $message .= '</tr>';
    $message .='</table>';
    $message .='</td>
                    </tr>
                    <tr>
                        <td height="25">&nbsp;</td>
                    </tr>
                    <tr>';
    $message .='<td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td>Thanks and Regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';
    $body = getNotificationTheme($siteURL . $subject, $message, '');
    $check = SignupSendMail($to, $body, $subject);
    return $check;
}


 function send_enquery_reply($username,$useremail,$subject,$product_services,$content,$qantity,$unit_name,$other){
	//print_r($username);die;
    $title = SITE_NAME;
    $subject = 'Enquiry Response Mail';
    $to = $useremail;
    $name = $username;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30dsfsd" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Hi ' . ucfirst(@$name) . ',</strong>
                        </td>
                    </tr>';
    $message .='<tr>
                    <td height="5" style="color:#f00;padding-left:5px;font-size:13px">'.$other.'
                                                
                    </td>

                </tr>';
                                    
    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px">Product/Services:'.$product_services.'
                    
                    </td>

                </tr>';
    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px"><b>Description:</b>'.$content.'<br>
                    <b>Quantity:</b> '.$qantity.' '.$unit_name.'
                    
                    </td>
                    </tr>';
                                    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td style="padding-left:5px">Thanks and regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);

    $check = sendMail($to, $body, $subject);

    return $check;
 }


 function send_complaint($username,$company_name,$useremail,$country,$country_code,$mobile,$content,$state){
	//print_r($username);die;
    $title = SITE_NAME;
    $subject = 'User Complaint';
    //$to = $useremail;
    $to = "admin@biznetafrica.com";
    //$to = INFO_EMAIL;
    
    $name = $username;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Hello Biznet Africa,</strong>
                        </td>
                    </tr>
		    <tr>
                        <td height="30" colspan="2" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Biznet Africa got following complaint: </strong>
                        </td>
                    </tr>';
     $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px"><div style="font-family:Tahoma, Arial, sans-serif;line-height:20px;margin-left:10">
		      '.$content.'<br/><br/>
		     
			<b>From:</b><br/><br/>
			Name: <b>'.ucfirst(@$name).'</b> <br/>
			Company Name:<b> '.$company_name.'</b> <br/>
			Email ID:<b> '.$useremail.'</b><br/>
                        Country:<b> '.$country.'</b><br/>
			Mobile:<b> '.$country_code.'-'.$mobile.'</b><br/>
			State:<b> '.$state.'</b><br/>
		   
                    </div>
                  </td>
		    
                </tr>';
    
    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td style="padding-left:5px">Thanks and Regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);
    
    //$cc = array(EMAIL_ADMIN);
    $check = sendMail($to, $body, $subject, $cc);

    return $check;
 }

// Send feedback in front end
function send_feedback($username,$company_name,$useremail,$country,$country_code,$mobile,$content,$state){
	//print_r($username);die;
    $title = SITE_NAME;
    $subject = 'User Feedback';
    //$to = $useremail;
    //$to = "biznetafrica@mailinator.com";
    $to = 'admin@biznetafrica.com';
    
    $name = $username;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Hello Biznet Africa,</strong>
                        </td>
                    </tr>

                    <tr>
                        <td height="30" colspan="2" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Biznet Africa got following feedback:</strong>
                        </td>
                    </tr>';
		  

    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px"><div style="font-family:Tahoma, Arial, sans-serif;line-height:20px;margin-left:10">
		      '.$content.'<br><br/>
		     
			<b>From:</b><br/><br/>
			Name:<b> '.ucfirst(@$name).'</b> <br/>
			Company Name:<b> '.$company_name.'</b> <br/>
			Email ID:<b> '.$useremail.'</b><br/>
                        Country:<b> '.$country.'</b><br/>
			Mobile:<b> '.$country_code.'-'.$mobile.'</b><br/>
			State: <b>'.$state.'</b><br/>
		   
                    </div>
                  </td>
		    
                </tr>';
    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td style="padding-left:5px">Thanks and Regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);
    
    //$cc = array(EMAIL_ADMIN);
    $check = sendMail($to, $body, $subject, $cc);

    return $check;
 }


 //Send Tender Request in front end
function tender_request_mail($username,$useremail,$mobile,$country,$tender_title,$tender_desc){
	//print_r($username);die;
    $title = SITE_NAME;
    $subject = 'User Tender Request';
    //$to = $useremail;
    //$to = "biznetafrica@mailinator.com";
    $to = ADMIN_EMAIL;
    
    $name = $username;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Hello Biznet Africa,</strong>
                        </td>
                    </tr>

                    <tr>
                        <td height="30" colspan="2" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Biznet Africa got following tender request:</strong>
                        </td>
                    </tr>';
		  

    $message .='<tr>
    			    <td height="5" style="padding-left:5px;font-size:13px"><div style="font-family:Tahoma, Arial, sans-serif;line-height:20px;margin-left:10">
    			   Tender Title:<b> '.$tender_title.'</b><br/><br/>

		     	 '.$tender_desc.'<br><br/>
		     
			<b>From:</b><br/><br/>
			Name:<b> '.ucfirst(@$name).'</b> <br/>
			Email ID:<b> '.$useremail.'</b><br/>
            Mobile:<b> '.$mobile.'</b><br/>
            Country:<b> '.$country.'</b><br/>
		   
                    </div>
                  </td>
		    
                </tr>';
    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td style="padding-left:5px">Thanks and Regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);

    $check = sendMail($to, $body, $subject);

    return $check;
 }


//Send Publish Tender Notification in Admin Panel
function publish_tender_mail($username,$tende_status,$tender_heading,$authority_type,$industry_type,$publish_date,$document_end_date, $tender_due_date, $tender_fees, $tender_details){
    $title = SITE_NAME;
    $subject = 'For Publish Tender';
    //$to = $useremail;
    //$to = "biznetafrica@mailinator.com";
    $to = ADMIN_EMAIL;
    
    $name = $username;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                           <strong>Hello '.ucfirst($name).',</strong> <br><br><br>
                           Your Tender request is approved by Admin

                        </td>
                    </tr>


                    <tr>
                        <td height="30" colspan="2" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>The following tender details is:</strong>
                        </td>
                    </tr>';
          

    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px"><div style="font-family:Tahoma, Arial, sans-serif;line-height:20px;margin-left:10">
                   Tender Title:<b> '.$tender_heading.'</b><br/><br/>

                 '.$tender_details.'<br><br/>
             
            <b>Tender is:</b><br/><br/>
            Tender Status:<b>'.$tende_status.'</b><br/>
            Authority Type:<b> '.$authority_type.'</b> <br/>
            Industry Type:<b> '.$industry_type.'</b><br/>
            Publish Date:<b> '.$publish_date.'</b><br/>
            Tender Document Sales End:<b> '.$document_end_date.'</b><br/>
            Due Date:<b> '.$tender_due_date.'</b><br/>
            Tender Fees:<b> '.$tender_fees.'</b><br/>
           
                    </div>
                  </td>
            
                </tr>';
    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td style="padding-left:5px">Thanks and Regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);

    $check = sendMail($to, $body, $subject);

    return $check;
 }



 function send_order_notification($userdata,$data){
    $title = SITE_NAME;
    $subject = 'Order Mail';
    $to = $userdata['userdetails']['user_primary_email']; //$userdata['userdetails']['user_primary_email']
    $name = $userdata['userdetails']['user_name'];
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30dsfsd" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Hi ' . ucfirst(@$name) . ',</strong>
                        </td>
                    </tr>';
                                    
    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px">Product/Services:'.$data['product_name'].'
                    
                    </td>

                </tr>';
    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px"><b>Description:</b>'.$data['product_desc'].'<br>
                    <b>Package Name:</b> '.$data['package_name'].'
                    
                    </td>
                    </tr>';
                                    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">';
    $message .= '<td style="padding-left:5px">Thanks and regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);
    //$cc = array(EMAIL_ADMIN);
    $check = sendMail($to, $body, $subject, $cc);

    return $check;
 }

//Send Tender Request in front end
function send_product_enq($sender_name,$owner_name, $receiver_email,$product_name){
    //print_r($username);die;
    $title = SITE_NAME;
    $subject = 'Product Enquiry';
    //$to = "biznetafrica@mailinator.com";
    $to = $receiver_email;
    //$to = "test@mailinator.com";
    
    $name = $sender_name;
    $siteURL = HTTP_PATH;

    $message = '';
    $message .='<tr>
                <td bgcolor="#2182B7" style="padding-left:5px;font-family:segoe UI, Arial, sans-serif; font-size:12px; color:#FFF; padding:6px 10px;">
                   <font style="font-size:15px;">' . $subject . '</font>
                </td>
                </tr>';
    $message .= '<tr>';
    $message .= '<td valign="top" bgcolor="#ffffff" style="padding:12px;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        <strong>Hello '.ucfirst(@$owner_name).',</strong>
                        </td>
                    </tr>

                    <tr>
                        <td height="30" colspan="2" style="padding-left:5px;font-family:Tahoma, Arial, sans-serif;font-size:12px;color:#575757;">
                        </td>
                    </tr>';
          

    $message .='<tr>
                    <td height="5" style="padding-left:5px;font-size:13px"><div style="font-family:Tahoma, Arial, sans-serif;line-height:20px;margin-left:10">
                   You received a new enquiry for your Product <b>'."'".ucfirst(@$product_name)."'".'</b> from <b>'.ucfirst(@$name).'.</b><br/><br><br>
                    Please <a href="' . HTTP_PATH . 'enquiry">Click Here</a> for viewing the product enquiry.
                    </div>
                  </td>
            
                </tr>';
    
    $message .='<tr><td>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr style="color:black;">

                ';
    $message .= '<td style="padding-left:5px">Thanks and Regards,<br />';
    $message .= '<a href="' . HTTP_PATH . '">' . SITE_NAME . '</a><br />';
    $message .= '</td></tr>';
    $message .= '</table>';
    $message .= '</tr>';

    $body = getNotificationTheme($title, $subject, $message);

    $check = sendMail($to, $body, $subject);

    return $check;
 }
