<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front/Model_users');
		$this->load->helper(array('form', 'url','common','email'));
		$data_array = array();
	}

	/*===========================================================
	* Function Start index (home default)
	*============================================================
	*/
	public function index() {
        $data_array['site_content']['display_file'] = 'front/home/body';
		$data_array['site_content']['css_file'] = 'stylesheet';
		$data_array['site_content']['title'] = 'Home';
        $data_array['site_content']['js_file'] = 'main';
        $this->load->view('front/template/template', $data_array);
	}
}
?>