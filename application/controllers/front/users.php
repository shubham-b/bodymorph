<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front/Model_users','MU');
		$this->load->helper(array('form', 'url','common','email'));
		$this->load->library('form_validation');
		$data_array= array();
	}

	/*
    Controller : contollers/front/users.php
	Function : user_login (user's login)
	Input Param : user's email, user's password
	Created By : Shubham Bhardwaj
	*/
	public function is_loged_in() {
        if (!$this->session->userdata('user_id')){
            redirect(HTTP_PATH);
        }
    }
    
    /*
    Controller : contollers/front/users.php
	Function : user_login (user's login)
	Input Param : user's email, user's password
	Created By : Shubham Bhardwaj
	*/
	public function user_login(){
		// echo "<pre>";print_r($_POST);die;
		if($this->input->post('username', TRUE) && $this->input->post('password', TRUE)){
			$this->form_validation->set_rules('username', 'Username', 'required|valid_email|callback_check_user_email');
	        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');

	        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
	        
	        if ($this->form_validation->run() == TRUE){  
	            $username = htmlentities(strip_tags($this->input->post('username')));
	            $password = strip_tags($this->input->post('password'));

	            $login_user_details = $this->MU->get_user_details($username, $password);
	            // print_r($this->db->last_query());die;
	            if($login_user_details){
	             	$session_array = array(
				                'user_id' => $login_user_details['user_id'],
				                'first_name' => $login_user_details['user_first_name'],
				                'last_name' => $login_user_details['user_last_name']
			                );
	                $this->session->set_userdata($session_array);
	                echo json_encode(array('errors' => 0));
	            }else{
	                echo json_encode(array('errors' => 1, 'message' => array('password' => 'Invalid Password', 'username' => '')));
	            }
	        }else{
	            echo json_encode(array('errors' => 1, 'message' => array('password' => form_error('password'), 'username' => form_error('username'))));
			}
		}else{
			echo json_encode(array('errors' => 1));
		}
    }

    /*
    Controller : contollers/front/users.php
	Function : check_user_email (user's login funtion username callback)
	Input Param : user's email
	Created By : Shubham Bhardwaj
	*/
	function check_user_email($email){
		if($this->MU->check_email($email)){
            return TRUE;
        }else{
            $this->form_validation->set_message('check_user_email', 'Email does not exists');
            return FALSE;
        }
	}

	/*
    Controller : contollers/front/users.php
	Function : forgot_password (user's forgot password)
	Input Param : user's email
	Created By : Shubham Bhardwaj
	*/
	public function forgot_password(){
		$this->form_validation->set_rules('old_password', 'Old Password', 'required|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('cnew_password', 'Conform Password', 'required|min_length[6]|max_length[15]');

        if ($this->form_validation->run() == TRUE){  
        	$post_data = $this->input->post();
        	if($post_data['new_password'] == $post_data['cnew_password']){
        		$user_password = $this->Model_users->user_password($email);
        		if(base64_decode($user_password['user_password']) == $post_data['old_password']){
        			if($this->Model_users->set_new_password($email, $post_data['new_password'])){
        				$response = array('message' => 'Password Changed Successfully');
        			}else{
        				$response = array('message' => 'Sorry! Something went wrong');
        			}
	        	}else{
	        		$response = array('message' => 'Old Password is Wrong');
	        	}
        	}else{
        		$response = array('message' => 'New & Conform Password does not match');
        	}
        }
        echo json_encode($response);
	}
    
    /*
    Controller : contollers/front/users.php
	Function : registration (user's registration)
	Input Param : user's registarion info
	Created By : Amit K Sharma
	*/
	public function registration() {	

		$this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[4]|max_length[49]');
        $this->form_validation->set_rules('sur_name', 'Surname', 'required|min_length[4]|max_length[49]');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[20]');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
  //       // $this->form_validation->set_rules('start_date', 'Start Date', 'required');
        // $this->form_validation->set_rules('first_date', 'First Date', 'required');
        $this->form_validation->set_rules('date_of_birth', 'Date Of Birth', 'required');
        $this->form_validation->set_rules('country', 'Country','required');
        $this->form_validation->set_rules('postcode', 'Postcode', 'required|integer');
        $this->form_validation->set_error_delimiters('<div class="alert-danger">', '</div>');
        
        if ($this->form_validation->run() == true){
			$user_details = array(
					// 'username' => $this->input->post('username'),
					'user_first_name' => $this->input->post('first_name'),
                    'user_last_name' => $this->input->post('sur_name'),
                    'user_email' => $this->input->post('user_email'),
                    'user_mobile' => $this->input->post('mobile'),
                    'user_email_token'=> str_rand(10, 'alphanumcaps'),
                    'user_password' => base64_encode($this->input->post('password')),
                    'user_gender' => $this->input->post('gender'),
                    // 'first_date' => $this->input->post('first_date'),
                    'user_dob' => $this->input->post('date_of_birth'),
                    'user_country_id' => $this->input->post('country'),
                    'user_postal_code' => $this->input->post('postcode'),
                    'know_about_us' => $this->input->post('know_about_us'),
                    
                );
		    $ret = $this->MU->registration_submit($user_details);

		    if($ret){
		    	$this->session->set_flashdata("success","<span class='text-success'>Registration Successfull, Verfication mail sent to your email, please verify</span>");
		    }

		    redirect('Registration');
        }else{
	        $data_array['site_content']['display_file'] = 'front/registration/register';
	        $data_array['site_content']['css_file'] = 'registration';
	        $data_array['site_content']['js_file'] = 'registration';
	        $data_array['site_content']['title'] = 'Register';
			
			$data_array['site_content']['countries']=$this->MU->get_country();

	        $this->load->view('front/template/template', $data_array);
    	}
	}
	
    /*
    Controller : contollers/front/users.php
	Function : registration (user's registration)
	Input Param : user's registarion info
	Created By : Amit K Sharma
	*/

    /*
    Controller : contollers/front/users.php
	Function : about
	Input Param : about us info
	Created By : Amit K Sharma
	*/
	public function about() {
        $data_array['site_content']['display_file'] = 'front/about/about';
        $data_array['site_content']['css_file'] = 'stylesheet';
        $data_array['site_content']['js_file'] = 'about';
        $data_array['site_content']['title'] = 'About';
		$data_array['site_content']['countries']=$this->MU->get_country();
        $this->load->view('front/template/template', $data_array);
	}
	
    /*
    Controller : contollers/front/users.php
	Function : about
	Input Param : about us info
	Created By : Amit K Sharma
	*/

    /*
    Controller : contollers/front/users.php
	Function : Blog
	Input Param : Blogx
	Created By : Amit K Sharma
	*/
	public function Blog() {
        $data_array['site_content']['display_file'] = 'front/blog/blog';
        $data_array['site_content']['css_file'] = 'stylesheet';
        $data_array['site_content']['js_file'] = 'blog';
        $data_array['site_content']['title'] = 'Blog';
		$data_array['site_content']['countries']=$this->MU->get_country();
        $this->load->view('front/template/template', $data_array);
	}
	
    /*
    Controller : contollers/front/users.php
	Function : Blog
	Input Param : Blog
	Created By : Amit K Sharma
	*/

    /*
    Controller : contollers/front/users.php
	Function : Gallery
	Input Param : Gallery
	Created By : Amit K Sharma
	*/
	public function Gallery() {
        $data_array['site_content']['display_file'] = 'front/gallery/gallery';
        $data_array['site_content']['css_file'] = 'stylesheet';
        $data_array['site_content']['js_file'] = 'gallery';
        $data_array['site_content']['title'] = 'Gallery';
		$data_array['site_content']['countries']=$this->MU->get_country();
        $this->load->view('front/template/template', $data_array);
	}
	
    /*
    Controller : contollers/front/users.php
	Function : Gallery
	Input Param : Gallery
	Created By : Amit K Sharma
	*/	

    /*
    Controller : contollers/front/users.php
	Function : Contact
	Input Param : Contact
	Created By : Amit K Sharma
	*/
	public function Contact() {
        $data_array['site_content']['display_file'] = 'front/contact/contact';
        $data_array['site_content']['css_file'] = 'stylesheet';
        $data_array['site_content']['js_file'] = 'contact';
        $data_array['site_content']['title'] = 'Contact';
		$data_array['site_content']['countries']=$this->MU->get_country();
        $this->load->view('front/template/template', $data_array);
	}
	
    /*
    Controller : contollers/front/users.php
	Function : Contact
	Input Param : Contact
	Created By : Amit K Sharma
	*/		

	//==========================Function Start submit_registration =================================
    /*
    Controller : contollers/front/users.php
	Function : registration (user's registration)
	Input Param : user's registarion info
	Created By : Amit K Sharma
	*/


	// public function submit_registration() {
	// 	 // echo '<pre>';print_r($_POST);die;
	// 	// $this->form_validation->set_rules('user_email', 'User Name', 'required|min_length[4]|max_length[49]');
	// 	$this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[4]|max_length[49]');
 //        $this->form_validation->set_rules('sur_name', 'Surname', 'required|min_length[4]|max_length[49]');
 //        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
 //        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[20]');
 //        $this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[10]');
 //        $this->form_validation->set_rules('gender', 'Gender', 'required');
 //  //       // $this->form_validation->set_rules('start_date', 'Start Date', 'required');
 //        // $this->form_validation->set_rules('first_date', 'First Date', 'required');
 //        $this->form_validation->set_rules('date_of_birth', 'Date Of Birth', 'required');
 //        $this->form_validation->set_rules('country', 'Country','required');
 //        $this->form_validation->set_rules('postcode', 'Postcode', 'required|integer');
 //        $this->form_validation->set_error_delimiters('<div class="alert-danger">', '</div>');
        
 //        if ($this->form_validation->run() == true){
	// 		$user_details = array(
	// 				// 'username' => $this->input->post('username'),
	// 				'user_first_name' => $this->input->post('first_name'),
 //                    'user_last_name' => $this->input->post('sur_name'),
 //                    'user_email' => $this->input->post('user_email'),
 //                    'user_mobile' => $this->input->post('mobile'),
 //                    'user_email_token'=> str_rand(10, 'alphanumcaps'),
 //                    'user_password' => base64_encode($this->input->post('password')),
 //                    'user_gender' => $this->input->post('gender'),
 //                    // 'first_date' => $this->input->post('first_date'),
 //                    'user_dob' => $this->input->post('date_of_birth'),
 //                    'user_country_id' => $this->input->post('country'),
 //                    'user_postal_code' => $this->input->post('postcode'),
 //                    'know_about_us' => $this->input->post('know_about_us'),
                    
 //                );
	// 	    $ret = $this->MU->registration_submit($user_details);

	// 	    if($ret){
	// 	    	$this->session->set_flashdata("success","Registration Successfull, Verfication mail sent to your email, please verify");
	// 	    }

	// 	    redirect('Registration');
 //        }	

	// }
	
	/*
    Controller : contollers/front/users.php
	Function : submit_registration (user's submit_registration)
	Input Param : user's registarion info
	Created By : Amit K Sharma
	*/
	//========================Function End successRegistration===========================
	

	//=========================Function Start userLogin ==================================
	// public function userLogin() {
	// 	$this->data_arry['main_content'][] = 'front/home'; 
 //        $this->load->view('front/includes/template', $this->data_arry);
	// }
	//===================== Function End userLogins=====================



	//=========================Function Start dashboard ==================================
    public function dashboard() {
    	// $data_array['site_content']['display_file'] = 'front/home/body';
		$data_array['site_content']['css_file'] = 'stylesheet';
		$data_array['site_content']['title'] = 'Dashboard';
        $data_array['site_content']['js_file'] = 'main';
        $data_array['site_content']['display_file'] = 'front/dashboard/dashboard'; 
        $this->load->view('front/template/template', $data_array);
    }
	//===================== Function End dashboard=====================
	
	// ========= Start supplier logout function ====================
    public function logout() {
        $session_array = array(
            'user_id' => "",
            'first_name' => "",
            'sur_name' => "",
            );
        $this->session->unset_userdata($session_array);
        $this->session->set_flashdata('message', '<div class="text-success">User logout successfully.</div>');
        redirect(HTTP_PATH);
    }   
    // ========= End supplier logout function ====================

    //=========================Function Start userProfile ==================================
	public function pageNotFound() {
		$this->isLoggedIn();
		$data_array['site_content']['display_file'] = 'front/page_not_found'; 
        $this->load->view('front/template/template', $this->data_arry);
	}
	//===================== Function End userProfile=====================


   //=========================Function Start userProfile ==================================
	public function userProfile() {
		$this->isLoggedIn();
        $this->data_arry['userprofile'] = $this->Model_users->getUserProfile($this->session->userdata('user_id'));	

        if(empty($this->data_arry['userprofile'])){
        	redirect(HTTP_PATH.'Page-Not-Found');
        }
        $this->data_arry['main_content'][] = 'front/user_profile'; 
        $this->load->view('front/includes/template', $this->data_arry);
        
	}
	//===================== Function End userProfile=====================
} // End Class
?>