		
			<div class="slider animated fadeIn wow">
				<div class="">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="<?php echo FRONT_IMG_PATH;?>s1.jpg" alt=""/>
								<div class="carousel-caption">
									<div class="container inner-wrapper">
										<div class="">
											<div class="slider-text">
												<div class="col-md-8 col-sm-8 slider-left-box">
													<div class="row">
														<h1>The Evolution Of Healthy Living</h1>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<!--cousel text--> 
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>s2.jpg" alt=""/>
								<div class="carousel-caption">
									<div class="container inner-wrapper">
										<div class="">
											<div class="slider-text">
												<div class="col-md-8 col-sm-8 slider-left-box">
													<div class="row">
														<h1>The Evolution Of Healthy Living</h1>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<!--cousel text--> 
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>s3.jpg" alt=""/>
								<div class="carousel-caption">
									<div class="container inner-wrapper">
										<div class="">
											<div class="slider-text">
												<div class="col-md-8 col-sm-8 slider-left-box">
													<div class="row">
														<h1>The Evolution Of Healthy Living</h1>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<!--cousel text--> 
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>s4.jpg" alt=""/>
								<div class="carousel-caption">
									<div class="container inner-wrapper">
										<div class="">
											<div class="slider-text">
												<div class="col-md-8 col-sm-8 slider-left-box">
													<div class="row">
														<h1>The Evolution Of Healthy Living</h1>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<!--cousel text--> 
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>s5.jpg" alt=""/>
								<div class="carousel-caption">
									<div class="container inner-wrapper">
										<div class="">
											<div class="slider-text">
												<div class="col-md-8 col-sm-8 slider-left-box">
													<div class="row">
														<h1>The Evolution Of Healthy Living</h1>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<!--cousel text--> 
							</div>
						</div>
						<!-- Controls --> 
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <i class="fa fa-chevron-left"></i></a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <i class="fa fa-chevron-right"></i> </a> 
					</div>
				</div>
				<div class="login-box">
					<div class="container inner-wrapper">
						<div class="col-md-4 col-sm-4 slider-right-box">
							<div class="row">
								<div class="login-reg-box">
									<h2>Login & Register</h2>
									<form method="post" id="userLogin" action="javascript:void(0);">
						  				<div class="form-group" >
										    <input type="text" class="form-control validate[required]" name="username" placeholder="Email" tabindex="1">
										    <span id="username" class="error"></span>
										</div>
						  				<div class="form-group">
										    <input type="password" class="form-control validate[required]" name="password" placeholder="Password" tabindex="2">
										    <span id="password" class="error"></span>
										</div>
				   					    <div class="checkbox">
						   					<input type="checkbox" class="css-checkbox" id="checkbox1" />
											<label for="checkbox1" name="checkbox1_lbl" class="css-label lite-green-check">Remember</label>
						  				</div>
						  				<div class="blue-btn">
						  					<button type="submit" id="user_login" name="login" class="btn">Login</button>
										    <a href="Registration" class="btn" >Sign Up</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--slider-->
			<section class="second-section animated fadeIn wow">
				<div class="container inner-wrapper">
					<div class="left-img col-md-6 col-sm-6">
						<div class="row">
							<img src="<?php echo FRONT_IMG_PATH;?>img1.jpg" alt=""/>
						</div>
					</div>
					<div class="right-section col-md-6 col-sm-6">
						<div class="row">
							<p>Understand the needs of your body type to get results fast. Look and feel your best with these revolutionary BodyMorph features</p>
							<ul>
								<li>
									<h4>Nutritional Needs Analysis</h4>
									<p>Key information that you enter about yourself and your goals can be used to calculate your exact calorie needs and the most effective macro-nutrient split to transform your body. This information is integrated into our meal plans to calculate your exact portion sizes for every meal in your plan. </p>
								</li>
								<li>
									<h4>Professional Training Plans For All Skill Levels;</h4>
									<p>No matter your experience, goals or skill levels there is a training program to suit you. All programs are result specific, modifiable and fun. Certain training types are best suited to different body types and we guide you towards what will work best.</p>
								</li>
								<li>
									<h4>A Dedicated Trainer To Ensure You're On The Right Track</h4>
									<p>Every BodyMorph user is assigned a Dedicated Health Strategist to walk hand in hand with them every step of their journey towards great results. They will aid you in choosing, adjusting and understanding your programs. They are equipped with knowledge and experience to keep you motivated, check-in on your progress and answer all of your questions</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</section>
			<section class="fit-slider animated fadeIn wow">
				<div class="container inner-wrapper">
					<div class="slider-inner">
						<div id="fit-slider">
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img2.jpg" alt="Owl Image">
								<h5>Understanding your body type</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img3.jpg" alt="Owl Image">
								<h5>BodyMorph Training Programs</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img4.jpg" alt="Owl Image">
								<h5>Understanding your body type</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img5.jpg" alt="Owl Image">
								<h5>BodyMorph Nutritional Plans</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img2.jpg" alt="Owl Image">
								<h5>Understanding your body type</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img3.jpg" alt="Owl Image">
								<h5>BodyMorph Training Programs</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img4.jpg" alt="Owl Image">
								<h5>Understanding your body type</h5>
							</div>
							<div class="item">
								<img src="<?php echo FRONT_IMG_PATH;?>img5.jpg" alt="Owl Image">
								<h5>BodyMorph Nutritional Plans</h5>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="testimonial animated fadeIn wow">
				<div class="testimonial-img">
					<div class="container inner-wrapper">
						<div class="heading">
							<h1>Testimonials</h1>
							<h3>What our clients say about us</h3>
						</div>
						<!--heading ends-->
						<div class="test-slider">
							<div id="owl-demo" class="owl-carousel owl-theme">
								<div class="item">
									<div class="test-img"><img src="<?php echo FRONT_IMG_PATH;?>test1.jpg" alt=""/></div>
									<div class="text-text">
										<div class="date">february 2nd, 2015</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  Maker including versions of Lorem Ipsum.</p>
										<div class="client-name">sjpaladino, Themeforest member</div>
									</div>
								</div>
								<div class="item">
									<div class="test-img"><img src="<?php echo FRONT_IMG_PATH;?>test1.jpg" alt=""/></div>
									<div class="text-text">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  Maker including versions of Lorem Ipsum.</p>
									</div>
								</div>
								<div class="item">
									<div class="test-img"><img src="<?php echo FRONT_IMG_PATH;?>test1.jpg" alt=""/></div>
									<div class="text-text">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  Maker including versions of Lorem Ipsum.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="blog-section animated fadeIn wow">
				<div class="container inner-wrapper">
					<div class="heading">
						<h1>Blog</h1>
						<h3>Recent Blog Entries</h3>
					</div>
					<!--heading ends-->
					<div class="blog-content">
						<div class="col-md-4 col-sm-4 blog-box">
							<div class="blog-image">
								<img src="<?php echo FRONT_IMG_PATH;?>b1.jpg" alt=""/>
							</div>
							<!--blog-image-->
							<div class="blog-text">
								<div class="blog-top">
									<div class="blog-left">
										<span class="month">June 6</span>
										<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
										<div class="clearfix"></div>
									</div>
									<div class="blog-right">
										<i class="fa fa-heart-o"></i> 5
									</div>
									<div class="clearfix"></div>
								</div>
								<h5>Need workout motivation? Try a virtual partner </h5>
								<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
							</div>
							<!--blog-text-->
						</div>
						<div class="col-md-4 col-sm-4 blog-box">
							<div class="blog-image">
								<img src="<?php echo FRONT_IMG_PATH;?>b2.jpg" alt=""/>
							</div>
							<!--blog-image-->
							<div class="blog-text">
								<div class="blog-top">
									<div class="blog-left">
										<span class="month">June 6</span>
										<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
										<div class="clearfix"></div>
									</div>
									<div class="blog-right">
										<i class="fa fa-heart-o"></i> 5
									</div>
									<div class="clearfix"></div>
								</div>
								<h5>10 healthiest tips for both men and women  </h5>
								<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
							</div>
							<!--blog-text-->
						</div>
						<div class="col-md-4 col-sm-4 blog-box">
							<div class="blog-image">
								<img src="<?php echo FRONT_IMG_PATH;?>b3.jpg" alt=""/>
							</div>
							<!--blog-image-->
							<div class="blog-text">
								<div class="blog-top">
									<div class="blog-left">
										<span class="month">June 6</span>
										<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
										<div class="clearfix"></div>
									</div>
									<div class="blog-right">
										<i class="fa fa-heart-o"></i> 5
									</div>
									<div class="clearfix"></div>
								</div>
								<h5>The do’s and don’ts of running for fitness </h5>
								<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
							</div>
							<!--blog-text-->
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</section>
			<section class="quote animated fadeIn wow">
				<div class="container inner-wrapper">
					<div class="pull-left quote-left">
						<h2>Lorem ipsum sit amet dollor is the dummy text</h2>
						<p>We recommend this layout for small or big companies. It is perfect for displaying your business values and attracting your visitors.</p>
					</div>
					<div class="pull-right quote-right blue-btn">
						<button class="btn">Get a quote</button>
					</div>
					<div class="clearfix"></div>
				</div>
			</section>
			