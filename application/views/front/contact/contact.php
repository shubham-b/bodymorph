<div class="banner animated fadeIn wow">
		<div class="banner-img">
		<img src="<?php echo FRONT_IMG_PATH;?>banner1.jpg" alt=""/>
		<div class="overlay">
		<div class="container inner-wrapper"><h1>Contact Us</h1>
		</div>
		</div>
		</div> <!--banner-img-->
		
		
	</div>
	<!--banner-->
	<div class="breadcrumb-custom">
	<div class="container inner-wrapper">
	<ol class="breadcrumb">
  <li>You are here:</li>
  <li><a href="<?php echo HTTP_PATH;?>">Home</a></li>
  <li class="active">Contact Us</li>
</ol>
	</div>
	</div>
<section class="contact-us animated fadeIn wow">
<div class="container inner-wrapper">
<div class="contact-top">
<div class="col-md-4 col-ms-4 contact-list">
<div class="">

<div class="contact-icon">

<i class="fa fa-phone"></i>
</div> <!--contact-icon ends-->
<div class="contact-text">Call us +01 568 2587</div>
<div class="clearfix"></div>
<p>Call us today. Our experts are here for you from 08:00 to 17:00 from Monday to Friday.</p>

</div>
</div>

<div class="col-md-4 col-ms-4 contact-list">
<div class="">
<div class="contact-icon">

<i class="fa fa-envelope-o"></i>
</div> <!--contact-icon ends-->
<div class="contact-text"><a href="#">Info@business.com</a></div>
<div class="clearfix"></div>
<p>Have a question regarding our services, please use the form below and send us an email. We’ll respond in 24h.</p>

</div>
</div>

<div class="col-md-4 col-ms-4 contact-list">
<div class="">
<div class="contact-icon">

<i class="fa fa-building-o"></i>
</div> <!--contact-icon ends-->
<div class="contact-text">Visit us at our office</div>
<div class="clearfix"></div>
<p>Want to make an apointment. Great, use the form bellow and we’ll get back to you in 24h.</p>

</div>
</div>
<div class="clearfix"></div>
</div> <!--contact-top-->
<div class="contact-box">
<div class="contact-info col-md-4 col-sm-4">
<div class="contact-heading">
<h5>INFO</h5>
<h2>Company information</h2>
</div> <!--contact-heading-->

<p>Vivamus vel lacinia eros. Aliquam porttitor sed nisi consequat auctor. Praesent volutpat tortor sit amet massa tristique sollicitudin. Pellentesque elit purus, sodales at pharetra vitae, condimentum at sapien. Curabitur justo dui, ornare id diam sit amet, condimentum vestibulum enim.</p>

<div class="address-box">
		<p class="add"><span>Address: </span> Some Street 123, Manhattan, <br>
New York, USA </p>
<p class="tel"><span> Telephone: </span>  +00 123 3456</p>
<p class="email"><span>Email: </span> <a href="#">  info@business.com </a></p>
		</div>
</div>
<div class="conatct-form col-md-8 col-sm-8">
<div class="contact-heading">
<h5>CONTACT</h5>
<h2>Get in touch</h2>
</div>

<form action="javascript:void(0)">
<div class="input-group half-width">
<label><span>*</span> Full Name</label>
  <div class="form-box"><span class="input-group-addon" ><i class="fa fa-pencil"></i></span>
  <input type="text" class="form-control"  aria-describedby="basic-addon1">
  </div>
</div>
 <!--input-group-->
	
	<div class="input-group half-width">
<label><span>*</span> Your Email address</label>
   <div class="form-box"><span class="input-group-addon" ><i class="fa fa-globe"></i></span>
  <input type="text" class="form-control"  aria-describedby="basic-addon1">
  </div>
</div>
 <!--input-group-->
 
 	<div class="input-group half-width">
<label>Link to your portfolio or personal website (Optional)</label>
  <div class="form-box"><span class="input-group-addon" ><i class="fa fa-link"></i></span>
  <input type="text" class="form-control"  aria-describedby="basic-addon1">
  </div>
</div>
 <!--input-group-->
 
 <div class="input-group full-width">
<label>Your message (Optional):</label>
   <div class="form-box"><span class="input-group-addon" ><i class="fa fa-envelope"></i></span>
  <textarea  class="form-control" aria-describedby="basic-addon1">
  </textarea>
  </div>
</div>
 <!--input-group-->
 
 <div class="blue-btn">
 <button class="btn">Submit</button>
 </div>

</form>
</div>





<div class="clearfix"></div>

</div> 
</div>
</section>






<section class="quote animated fadeIn wow">
<div class="container inner-wrapper">
<div class="pull-left quote-left">
<h2>Lorem ipsum sit amet dollor is the dummy text</h2>

<p>We recommend this layout for small or big companies. It is perfect for displaying your business values and attracting your visitors.</p>
</div>
<div class="pull-right quote-right blue-btn">
<button class="btn">Get a quote</button>
</div>
<div class="clearfix"></div>
</div>
</section>
