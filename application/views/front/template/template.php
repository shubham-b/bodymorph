<?php 	
	$files_array = array();
	$files_array = $site_content;

	if($files_array['css_file'] != '')
		$this->load->view('front/common_pages/head', $files_array);
	else
		$this->load->view('front/common_pages/head');

	$this->load->view('front/common_pages/header');
	
	if(isset($files_array['display_file']))
	  	$this->load->view($files_array['display_file'],$files_array);

	if($files_array['js_file'] != '')
		$this->load->view('front/common_pages/footer', $files_array);
	else
		$this->load->view('front/common_pages/footer');
?>