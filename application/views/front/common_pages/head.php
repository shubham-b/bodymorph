<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<title>::<?php echo $title; ?>::</title>
		<link href="<?php echo FRONT_CSS_PATH;?>bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo FRONT_CSS_PATH;?>animated.css" rel="stylesheet" type="text/css">
		<link href="<?php echo FRONT_CSS_PATH;?>owl.carousel.css" rel="stylesheet" type="text/css">
		<link href="<?php echo FRONT_CSS_PATH;?>bootstrap-select.css" rel="stylesheet" type="text/css">
		<link href="<?php echo FRONT_CSS_PATH;?>responsive.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo FRONT_CSS_PATH;?>font-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo FRONT_CSS_PATH;?>datepicker3.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo FRONT_CSS_PATH;?>validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo FRONT_CSS_PATH;?>common_css.css" rel="stylesheet" type="text/css" />

		<link href="<?php echo FRONT_CSS_PATH.$css_file.'.css';?>" rel="stylesheet" type="text/css" />

		<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<script type="text/javascript">
			var HTTP_PATH = '<?php echo HTTP_PATH; ?>';
		</script>
	</head>