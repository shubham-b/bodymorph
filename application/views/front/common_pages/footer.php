			<footer>
				<div class="top-footer animated fadeInUp wow">
					<div class="container inner-wrapper ">
						<div class="footer-content">
							<div class="col-md-3 col-sm-3 newslatter">
								<div class="">
									<h4>newsletter subscribe</h4>
									<p>Want to subscribe to our newsletter feed?</p>
									<p> Please fill the form below.</p>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Your email address">
										<span class="input-group-btn">
										<button class="btn btn-default" type="button"><i class="fa fa-envelope-o"></i></button>
										</span>
									</div>
									<!-- /input-group -->
								</div>
							</div>
							<div class="col-md-3 col-sm-3 information">
								<div class="">
									<h4>company information</h4>
									<ul>
										<li><a href="About"><i class="fa fa-angle-right"></i> About us</a></li>
										<li><a href="#"><i class="fa fa-angle-right"></i> Our Services</a></li>
										<li><a href="#"><i class="fa fa-angle-right"></i> Company News</a></li>
										<li><a href="#"><i class="fa fa-angle-right"></i> Legal Information</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 tweet">
								<div class="">
									<h4>LATEST TWEETS</h4>
									<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor <a href="#">http://t.co/AaY2KBbvvb</a></p>
									<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor<a href="#">  http://t.co/AaY2KBbvvb</a></p>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 address">
								<div class="">
									<h4>Get In touch</h4>
									<p class="add"><span>Address: </span> Some Street 123, Manhattan, 
										New York, USA 
									</p>
									<p class="tel"><span> Telephone: </span>  +00 123 3456</p>
									<p class="email"><span>Email: </span> <a href="#">  info@business.com </a></p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="bottom-footer">
					<div class="container inner-wrapper">
						<p>Body Morph Online Fitness @copyright All Right Researved</p>
					</div>
				</div>
			</footer>
		</div>
		<!--wrapper--> 
		<script src="<?php echo FRONT_SITE_JS_PATH;?>jquery-1.11.3.min.js" type="text/javascript" ></script> 
		<script src="<?php echo FRONT_SITE_JS_PATH;?>jquery-ui.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>bootstrap.js" type="text/javascript"></script>  
		<script src="<?php echo FRONT_SITE_JS_PATH;?>owl.carousel.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>wow.js"  type="text/javascript"> </script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>bootstrap-select.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>custom.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>jquery.validationEngine.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo FRONT_PAGE_JS_PATH.$js_file.'.js';?>" type="text/javascript"></script>
		<script src="<?php echo FRONT_SITE_JS_PATH;?>masonry.pkgd.min.js"></script>

	</body>
</html>