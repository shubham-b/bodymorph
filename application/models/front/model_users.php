<?php

class Model_users extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*
    Controller : models/front/model_users.php
    Function : get_user_details (user's details)
    Input Param : user's email, user's password
    Created By : Shubham Bhardwaj
    */
    public function get_user_details($username, $password){
        $this->db->select('user_id, user_first_name, user_last_name, user_email');
        $this->db->from('bm_users');
        $this->db->where(array(
                        'user_email' => $username, 
                        'user_password' => base64_encode($password),
                        'user_email_is_verified' => 'Y',
                        'user_status' => 'Y'
                    ));
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return FALSE;
        }
    }

    /*
    Controller : models/front/model_users.php
    Function : check_email (user's login funtion username callback)
    Input Param : user's email
    Created By : Shubham Bhardwaj
    */
    function check_email($email){
        $this->db->select('user_email');
        $this->db->from('bm_users');
        $this->db->where('user_email', $email);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function registration_submit($user_details) {
        $check = $this->db->insert('bm_users', $user_details);
        $userid = $this->db->insert_id();
        if ($userid) {
            return $userid;
        }
        else {
            return FALSE;
        }
    }


    public function get_country(){
        $this->db->select('country_id,country_name');
        $this->db->from('bm_country');
        $query = $this->db->get();
        if($query->num_rows()){
            return $query->result_array();
        }else{
            return false;
        }
    }
    


    public function getUserProfile($userId){
        $this->db->select('user.*, country.country_name');
        $this->db->from('bm_users user');
        $this->db->join('bm_country country', 'country.country_id=user.country_id');
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return FALSE;
        }
    }



    public function getAutoLoginDetails($email, $password){
        $this->db->select('user_id,first_name,sur_name,email,email_token,status');
        $this->db->from('bm_users');
        $this->db->where(array(
            'email' => $email, 
            'password' => md5($password),
            ));
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return FALSE;
        }
    }


    public function user_login($userid, $password) {
        $id = substr($userid, 0, 1);
        $this->db->select('user_name,user_primary_email,user_primary_mobile_number,user_id');
        $this->db->from('bm_users');
        if ($id == '+') {
            $mobile = substr($userid, 4);
            $code = substr($userid, 0, 4);
            if ($mobile == '' || $code == '') {
                return FALSE;
            }
            $this->db->join('tbl_country', 'tbl_user.country_id=tbl_country.country_id', 'inner');
            $this->db->where('tbl_country.country_dialing_code', $code);
            $this->db->where('tbl_user.user_primary_mobile_number', $mobile);
        }
        else {
            $where = "(user_primary_email='$userid')";
            $this->db->where($where);
        }
        $this->db->where('user_password', $password);
        $this->db->where('is_status', 1);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        $data = $query->row_array();
        return $data;
    }


    public function getUserData($userid) {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $where = "(user_primary_email='$userid' OR user_primary_mobile_number='$userid')";
        $this->db->where($where);
        $this->db->where('is_status', 1);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        $data = $query->row_array();
        return $data;
    }


    public function email_verification($email) {
        $data = array(
            'user_email_token' => '',
            'user_email_verification' => 1
        );
        $this->db->where('user_primary_email', $email);
        return $this->db->update('tbl_user', $data);
    }


    public function user_detail($userDetail,$user_id){
        $this->db->where('user_id',$user_id);
        $this->db->update('tbl_user_details',$userDetail);
        return true;
    }

    public function user_password($user_id){
        $this->db->select('user_password');
        $this->db->from('bm_user');
        $this->db->where('user_id', $user_id);
        return $this->db-get()->row_array();
    }

    public function set_new_password($user_id, $new_password){
        $this->db->where('user_id', $user_id);
        return $this->db->update('bm_user', array('user_password' => base64_encode($new_password)));
    }
}